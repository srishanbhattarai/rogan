extern crate reqwest;

pub struct Rogan;

impl Rogan {
    pub fn new() -> Rogan {
        Rogan {}
    }

    pub fn gitignore_for_language(&self, lang: &str) -> Option<String> {
        let url = format!("https://www.gitignore.io/api/{}", lang);

        // TODO: Error handling
        let contents = reqwest::get(&url).unwrap().text().unwrap();
        if contents.contains("undefined") {
            return None;
        }

        Some(contents)
    }

    pub fn list_languages(&self) -> Option<String> {
        let url = format!("https://www.gitignore.io/api/list");

        // TODO: Error handling
        let contents = reqwest::get(&url).unwrap().text().unwrap();

        Some(contents)
    }
}
