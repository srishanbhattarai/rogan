extern crate clap;
extern crate rogan;

use clap::{App, Arg};
use rogan::Rogan;

fn main() {
    let version = include_str!("../version");

    let app = App::new("Rogan")
        .version(version)
        .author("Srishan Bhattarai <srishanbhattarai@gmail.com>")
        .about("A gitignore manager for the command line")
        .arg(Arg::with_name("languages").required(true));
    let matches = app.get_matches();
    let lang = matches
        .value_of("languages")
        .expect("Expected languages to be valid");

    if lang == "ls" {
        show_lang_list();

        return;
    }

    show_gitignore(lang);
}

fn show_gitignore(lang: &str) {
    // Allow comma separated values for multiple languages.
    let langs: Vec<&str> = lang.split(",").collect();

    let rogan = Rogan::new();

    // TODO: Maybe multi-thread this?
    for l in langs {
        match rogan.gitignore_for_language(l) {
            Some(contents) => print!("{}", contents),
            None => {
                let err = format!("Could not get the gitignore file for: {}", l.to_string());

                eprintln!("{}", err);
            }
        }
    }
}

fn show_lang_list() {
    let rogan = Rogan::new();

    match rogan.list_languages() {
        Some(contents) => {
            println!("{}", contents);
        }
        None => {
            let err = format!("Could not get the list of languages");

            eprintln!("{}", err);
        }
    }
}
