# Rogan

A fast, simple and stateless version of [Joe](https://github.com/karan/joe), the `.gitignore` magician for the command line.

## Features 
* [x] Print `gitignore` for various languages to stdout
* [x] Add `ls` command to list out available languages
* [ ] Incorporate levenshtein distance to suggest languages in case of mistakes
* [ ] Multi-threaded (non-blocking) requests when querying multiple languages

## Usage
After installing the binary:

Print the `.gitignore` for a number of languages:
```
$ rogan java,go,rust
```

Show list of available languages:
```
$ rogan ls
```

In POSIX systems, use the following command to *append* the output to the `.gitignore` file.
```
$ rogan go,rust >> .gitignore
```

## License
MIT
